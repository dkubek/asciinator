import numpy as np


def asciify(im, glyphmap, width, chratio=(1, 2)):
    """
    Produce the visually closest character representation of given image.

    im:         ImagePixels object
    glyphmap:   GlyphMap object
    width:      Width and height of output image in characters
    chratio:    Character ratio (ch_width, ch_height).
        Depends on the font and viewing terminal.
        Default is (1, 2)

    Returns a string containing characters.
    """
    data = im.data
    # Height adaptative to image width and character ratio
    height = int(
        width * im.info.height / im.info.width * chratio[0] / chratio[1]
    )

    # Partition image into a grid and generate grid boundrie
    dy = np.linspace(0, im.info.height - 1, height + 1, dtype=np.uint32)
    dx = np.linspace(0, im.info.width - 1, width + 1, dtype=np.uint32)

    ascii = []
    for row in range(1, height + 1):
        for col in range(1, width + 1):
            area = data[dy[row - 1] : dy[row], dx[col - 1] : dx[col]]
            x = np.mean(area)
            ascii.append(glyphmap[x])
        ascii.append("\n")

    return "".join(ascii)
