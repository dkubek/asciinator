import numpy as np
from src.ImagePixels import ImagePixels


def linear_dist(start, end, k, dtype=np.uint8):
    """
    Distribute values liearly on interval [start, end].

    start:  start of interval
    end:    end of interval
    k:      number of values

    By default returns array of uint8.

    """

    if k == 0:
        return np.empty(dtype=dtype)

    values = np.empty(k, dtype=dtype)
    step = (end - start + 1) / k
    for i in range(k):
        values[i] = np.round(start + i * step)

    values[-1] = end

    # Or by using numpy's linspace function
    # values = np.linspace(start, end, k, dtype=np.uint8)

    return values


def posterize(im, k, mode="mean"):
    """
    Applies posterization to 3D numpy array. Posterization reduces the
    amount of colors in an image by splitting each color channel into ranges.

    im:     ImagePixels object
    k:      positive number of ranges
    mode:   mode to chose value_k by (default="mean")
        Supported modes: mean

    """

    # TODO: Implement different modes i.e, low, high, spread ...
    data = im.data

    values = linear_dist(0, 255, k)

    mult = np.uint16(k - 1)

    # Map value to correct interval for k-value
    def map_value(ch):
        key = (mult * np.uint16(ch)) >> 8
        return values[key]

    # Special case optimization for k value of 2
    def map_value2(ch):
        key = ch >> 7
        return values[key]

    if k == 2:
        return ImagePixels(map_value2(data), im.info)

    return ImagePixels(map_value(data), im.info)


def normalize(im, lower=0, upper=255):
    """
    Normalize the brightness range of an image by stretching it
    to full range of colors [0, 255]

    data:       Image data
    min, max:   optional interval into which to normalize colors

    Returns normalized data.

    """

    data = im.data

    # Get brightness range - i.e. darkest and lightest pixels
    min = np.min(data)
    max = np.max(data)

    table = np.zeros(256, dtype=np.uint8)
    table[min : max + 1] = np.linspace(
        lower, upper, (max - min) + 1, dtype=np.uint8
    )

    return ImagePixels(table[data], im.info)


def luma_coding(rgb):
    """
    Converts rgb value to luma (luminiscence value) value.
    More information: https://en.wikipedia.org/wiki/Grayscale

    rgb:    numpy array of size 3

    Returns a luminiscence value.

    """

    return np.dot(rgb, [0.299, 0.587, 0.114])


def grayscale(im, mode="mean"):
    """
    Convert image represented as numpy array to rgb-represented grayscale.

    im:     ImagePixels object
    mode:   string resentign mode how to choose grayscale value
        Supported modes:
            luma:   using linear luma algoritm
            mean:   averaging color channels

    """

    data = im.data

    gs = None
    if mode == "luma":
        gs = luma_coding(data)
    elif mode == "mean":
        gs = np.mean(data, axis=2)

    gs = gs.astype(np.uint8)
    gs = np.repeat(gs[:, :, np.newaxis], 3, axis=2)

    return ImagePixels(gs, im.info)


def invert(im):
    """
    Invert the colors in given ImagePixels.

    im:     ImagePixels object

    Returns new ImagePixels object.
    """
    data = im.data

    def inv(ch):
        return 255 - ch

    return ImagePixels(inv(data), im.info)
