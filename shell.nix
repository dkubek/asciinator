{ pkgs ? import <nixpkgs> {} }:

let
  poetry2nix = pkgs.poetry2nix;

  develEnv = poetry2nix.mkPoetryEnv {
    projectDir = ./.;

    editablePackageSources = {
      my-app = ./src;
    };
  };

 customVim = pkgs.vimHugeX.override {
   python = pkgs.python3.withPackages(ps: [
     ps.python-language-server
     ps.pyls-mypy ps.pyls-isort ps.pyls-black ps.jedi
   ]);
 };

  mcf-simplex-analyzer = import ./default.nix {};
in
  pkgs.mkShell {
    buildInputs = [
      pkgs.poetry
      develEnv
      customVim
    ];
  }
